<?php

include_once 'CommonDrupalPHPUnitTest.php';

/**
 *
 * @author zonkone
 */
class exampleTest extends CommonDrupalPHPUnitTest {

    public function __construct() {
        parent::__construct(null, null);
    }

    /**
     * Tests if node_load() works. You need a node with nid=1 to pass this test.
     * 
     */
    public function testDrupalNodeLoadDirect() {
        $this->assertFalse(node_load(1) instanceOf Object);
    }

    /**
     * Tests if the module's hook CommonDrupalPHPUnitTest_node_load can be accessed.
     * 
     */
    public function testDrupalNodeLoadModuleHook() {
        $this->assertTrue(CommonDrupalPHPUnitTest_node_load("", "1"));
    }

}

?>
