<?php

/**
 * Bootstraps the Drupal and sets up core functions (like node_load()) for use in PHPUnit test.
 * Extend your own testcase from the CommonDrupalPHPUnitTest and call the parrent::__constructor 
 * with the absolute path of the your Drupal installation and the remote adress of your host.
 *
 * @author zonkone
 */
class CommonDrupalPHPUnitTest extends PHPUnit_Framework_TestCase
{
    
    protected $drupalRoot = null;

    /**
     * Sets up the path to the Drupal root.
     *
     * @var $drupalRoot     | absolut path to your Drupal installation
     * @var $remoteAdress   | remote adress of your host 
     */
    public function __construct($drupalRoot=null, $remoteAdress=null) {
    	if($drupalRoot!=null && $remoteAdress!=null){
        	$this->drupalRoot = $drupalRoot;
            define('DRUPAL_ROOT', $drupalRoot);
            require_once (DRUPAL_ROOT.'./includes/bootstrap.inc');
            $_SERVER['REMOTE_ADDR'] = $remoteAdress;
            drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL); 
    	}
    }


	/**
	 * Test if the the drupal root path is set.
	 *
	 * @throws Exception | if path of drupal installation is not set.
	 */
	public function testInitializing(){
			if($this->drupalRoot == null){
				throw new Exception("Test could not be initialized: Please set up include path!");

			}
	}

}    


?>
